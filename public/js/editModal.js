$(document).ready(function () {
  $('#employeesTable').on('click', '.btn-warning', function () {
    var employeeId = $(this).data('id');

    $.ajax({
      url: `/panel/employee/${employeeId}/edit`,
      method: 'GET',
      success: function (response) {
        $('#editEmployeeModal .modal-body').html(`
                  <div class="mb-3">
                      <label for="employeeName" class="form-label">Name</label>
                      <input type="text" class="form-control" id="employeeName" name="name" value="${response.name}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeeIsActive" class="form-label">Status</label>
                      <select class="form-control" id="employeeIsActive" name="is_active" required>
                          <option value="1" ${response.is_active ? 'selected' : ''}>Active</option>
                          <option value="0" ${!response.is_active ? 'selected' : ''}>Inactive</option>
                      </select>
                  </div>
                  <div class="mb-3">
                      <label for="employeeIdentityNumber" class="form-label">Identity Number</label>
                      <input type="text" class="form-control" id="employeeIdentityNumber" name="identity_number" value="${response.identity_number}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeeGender" class="form-label">Gender</label>
                      <select class="form-control" id="employeeGender" name="gender" required>
                          <option value="male" ${response.gender === 'male' ? 'selected' : ''}>Male</option>
                          <option value="female" ${response.gender === 'female' ? 'selected' : ''}>Female</option>
                      </select>
                  </div>
                  <div class="mb-3">
                      <label for="employeeEmail" class="form-label">Email</label>
                      <input type="email" class="form-control" id="employeeEmail" name="email" value="${response.email}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeePhoneNumber" class="form-label">Phone Number</label>
                      <input type="text" class="form-control" id="employeePhoneNumber" name="phone" value="${response.phone}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeeAddress" class="form-label">Address</label>
                      <textarea class="form-control" id="employeeAddress" name="address">${response.address || ''}</textarea>
                  </div>
                  <div class="mb-3">
                      <label for="employeePosition" class="form-label">Position</label>
                      <input type="text" class="form-control" id="employeePosition" name="position" value="${response.position}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeeHireDate" class="form-label">Hire Date</label>
                      <input type="date" class="form-control" id="employeeHireDate" name="hire_date" value="${response.hire_date.substr(0, 10)}" required>
                  </div>
                  <div class="mb-3">
                      <label for="employeeSalary" class="form-label">Salary</label>
                      <input type="number" class="form-control" id="employeeSalary" name="salary" value="${response.salary}" required>
                  </div>
              `);
        $('#editEmployeeForm').attr('action', `/panel/employee/${employeeId}/update`);

        $('#editEmployeeModal').modal('show');
      },
      error: function (xhr, status, error) {
        console.error(xhr);

        Swal.fire({
          icon: 'error',
          title: 'Error!',
          text: 'Failed to fetch employee details. Please try again later.',
          showConfirmButton: true
        });
      }
    });
  });

  $('#editEmployeeForm').submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        $('#editEmployeeModal').modal('hide');
        $('#employeesTable').DataTable().ajax.reload();

        Swal.fire({
          icon: 'success',
          title: 'Success!',
          text: 'Employee updated successfully.',
          showConfirmButton: false,
          timer: 1500
        });
      },
      error: function (response) {
        console.error(response);

        if (response.status === 400) {
          var errors = response.responseJSON.errors;
          var errorMessage = "";

          $.each(errors, function (key, value) {
            errorMessage += value + "<br>";
          });

          Swal.fire({
            icon: 'error',
            title: 'Validation Error!',
            html: errorMessage,
            showConfirmButton: true
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: 'Failed to update employee. Please try again later.',
            showConfirmButton: true
          });
        }
      }
    });
  });
});
