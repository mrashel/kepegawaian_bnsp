$(document).ready(function () {
  $('#employeesTable').on('click', '.show-details', function () {
    var employeeId = $(this).data('id');

    $.ajax({
      url: `/panel/employee/${employeeId}/show`,
      method: 'GET',
      success: function (response) {
        var detailsHtml = `
              <div class="row">
                  <div class="col-md-8">
                      <div class="mb-3">
                          <h5>Personal Information</h5>
                          <hr>
                          <dl class="row">
                              <dt class="col-sm-4">ID:</dt>
                              <dd class="col-sm-8">${response.id}</dd>
                              
                              <dt class="col-sm-4">Name:</dt>
                              <dd class="col-sm-8">${response.name}</dd>
                              
                              <dt class="col-sm-4">Identity Number:</dt>
                              <dd class="col-sm-8">${response.identity_number}</dd>
                              
                              <dt class="col-sm-4">Gender:</dt>
                              <dd class="col-sm-8">${response.gender}</dd>
                              
                              <dt class="col-sm-4">Email:</dt>
                              <dd class="col-sm-8">${response.email}</dd>
                              
                              <dt class="col-sm-4">Phone Number:</dt>
                              <dd class="col-sm-8">${response.phone}</dd>
                              
                              <dt class="col-sm-4">Address:</dt>
                              <dd class="col-sm-8">${response.address || ''}</dd>
                          </dl>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="mb-3">
                          <h5>Employment Information</h5>
                          <hr>
                          <dl class="row">
                              <dt class="col-sm-4">Position:</dt>
                              <dd class="col-sm-8">${response.position}</dd>
                              
                              <dt class="col-sm-4">Hire Date:</dt>
                              <dd class="col-sm-8">${moment(response.hire_date).format('YYYY-MM-DD')}</dd>
                              
                              <dt class="col-sm-4">Salary:</dt>
                              <dd class="col-sm-8">${response.salary}</dd>
                              
                              <dt class="col-sm-4">Status:</dt>
                              <dd class="col-sm-8">${response.is_active ? 'Active' : 'Inactive'}</dd>
                          </dl>
                      </div>
                  </div>
              </div>
          `;

        $('#employeeDetails').html(detailsHtml);
        $('#showEmployeeModal').modal('show');
      }
    });
  });
});