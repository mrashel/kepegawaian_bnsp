// script for add modal with form validation
$(document).ready(function () {
  $('#addEmployeeForm').validate({
    rules: {
      name: {
        required: true,
        maxlength: 100
      },
      identity_number: {
        required: true,
        digits: true,
        minlength: 16,
        maxlength: 16
      },
      gender: {
        required: true
      },
      email: {
        required: true,
        email: true,
        maxlength: 100
      },
      phone: {
        required: true,
        maxlength: 13,
        digits: true
      },
      address: {
        maxlength: 255
      },
      position: {
        required: true,
        maxlength: 100
      },
      hire_date: {
        required: true
      },
      salary: {
        required: true,
        number: true,
        min: 0
      }
    },
    messages: {
      identity_number: {
        digits: "Please enter only digits for Identity Number.",
        minlength: "Identity Number must be exactly 16 digits long.",
        maxlength: "Identity Number must be exactly 16 digits long."
      },
      phone: {
        digits: "Please enter only digits for Phone Number."
      }
    },
    errorElement: 'div',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback text-danger');
      element.closest('.mb-3').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid').removeClass('is-valid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid').addClass('is-valid');
      $(element).closest('.mb-3').find('.invalid-feedback').remove();
    },
    submitHandler: function (form) {
      var formData = new FormData(form);
      var csrfToken = "{{ csrf_token() }}";

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': csrfToken
        }
      });

      $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          $('#addEmployeeModal').modal('hide');
          $('#employeesTable').DataTable().ajax.reload();

          Swal.fire({
            icon: 'success',
            title: 'Success!',
            text: 'Employee added successfully.',
            showConfirmButton: false,
            timer: 1500
          });

          // Clear the form fields after success submit
          $('#addEmployeeForm')[0].reset();
          $('#addEmployeeForm').find('.is-valid, .is-invalid')
            .removeClass('is-valid is-invalid');
        },
        error: function (response) {
          console.error(response);

          if (response.status === 400) {
            var errors = response.responseJSON.errors;
            var errorMessage = "";

            $.each(errors, function (key, value) {
              errorMessage += value + "<br>";
            });

            Swal.fire({
              icon: 'error',
              title: 'Validation Error!',
              html: errorMessage,
              showConfirmButton: true
            });
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Error!',
              text: 'Failed to add employee. Please try again later.',
              showConfirmButton: true
            });
          }
        }
      });
    }
  });

  // Clear form modal on hidden
  $('#addEmployeeModal').on('hidden.bs.modal', function () {
    $('#addEmployeeForm')[0].reset();
    $('#addEmployeeForm').find('.is-valid, .is-invalid').removeClass('is-valid is-invalid');
  });
});