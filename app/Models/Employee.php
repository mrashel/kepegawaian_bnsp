<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'identity_number',
        'gender',
        'email',
        'phone',
        'address',
        'hire_date',
        'position',
        'salary',
        'is_active',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'hire_date' => 'date',
    ];
}
