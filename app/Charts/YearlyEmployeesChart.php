<?php

namespace App\Charts;

use ArielMejiaDev\LarapexCharts\LarapexChart;
use App\Models\Employee;

class YearlyEmployeesChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\BarChart
    {
        $employees = Employee::selectRaw('YEAR(hire_date) as year, COUNT(*) as total')
            ->groupBy('year')
            ->orderBy('year')
            ->pluck('total', 'year');

        // Prepare data for chart
        $years = $employees->keys()->toArray();
        $totals = $employees->values()->toArray();

        return $this->chart->barChart()
            ->setTitle('Yearly Employees')
            ->setSubtitle('Number of employees hired each year')
            ->addData('Employees', $totals)
            ->setXAxis($years)
            ->setColors(['#ffc63b', '#ff6384'])
            ->setHeight(300)
            ->setFontFamily('Poppins')
            ->setFontColor('#373d3f');
    }
}
