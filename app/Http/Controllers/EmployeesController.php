<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeesController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dataEmployee = Employee::latest()->get();
            return response()->json([
                'data' => $dataEmployee
            ], 200);
        }

        $employees = Employee::all();
        return view('employees', compact('employees'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'identity_number' => 'required|string|size:16|unique:employees,identity_number',
            'gender' => 'required|in:male,female',
            'email' => 'required|email|max:100|unique:employees,email',
            'phone' => 'required|string|max:13|regex:/^[0-9\-\(\)\/\+\s]*$/',
            'address' => 'nullable|string|max:255',
            'hire_date' => 'required|date',
            'salary' => 'required|numeric|min:0',
            'position' => 'required|string|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $employee = Employee::create($validator->validated());

        return response()->json([
            'status' => 200,
            'data' => $employee
        ]);
    }


    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        return response()->json($employee, 200);
    }

    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->address = $employee->address ?? '';
        return response()->json($employee, 200);
    }

    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:100',
            'identity_number' => 'required|string|size:16|unique:employees,identity_number,' . $employee->id,
            'gender' => 'required|in:male,female',
            'email' => 'required|email|max:100|unique:employees,email,' . $employee->id,
            'phone' => 'required|string|max:13|regex:/^[0-9\-\(\)\/\+\s]*$/',
            'address' => 'nullable|string|max:255',
            'hire_date' => 'required|date',
            'salary' => 'required|numeric|min:0',
            'position' => 'required|string|max:100',
            'is_active' => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $employee->update($validator->validated());

        return response()->json(['status' => 200, 'message' => 'Employee updated successfully.']);
    }

    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();
        return response()->json(['status' => 200, 'message' => 'Employee deleted successfully.']);
    }
}
