<?php

namespace App\Http\Controllers;

use App\Charts\YearlyEmployeesChart;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(YearlyEmployeesChart $chart)
    {
        return view('home', ['chart' => $chart->build()]);
    }
}
