<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeesController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::prefix('panel')->middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/employees', [EmployeesController::class, 'index'])->name('employees.index');
    Route::post('/employee/store', [EmployeesController::class, 'store'])->name('employees.store');
    Route::get('/employee/{id}/show', [EmployeesController::class, 'show'])->name('employees.show');
    Route::get('/employee/{id}/edit', [EmployeesController::class, 'edit'])->name('employees.edit');
    Route::post('/employee/{id}/update', [EmployeesController::class, 'update'])->name('employees.update');
    Route::get('/employee/{id}/destroy', [EmployeesController::class, 'destroy'])->name('employees.destroy');
});
