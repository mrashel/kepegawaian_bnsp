<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Employee;
use Faker\Factory as Faker;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        foreach (range(1, 100) as $index) {
            Employee::create([
                'name' => $faker->name,
                'identity_number' => $faker->numerify('###########'),
                'gender' => $faker->randomElement(['male', 'female']),
                'email' => $faker->email,
                'phone' => $faker->numerify('#############'),
                'address' => $faker->address,
                'hire_date' => $faker->date('Y-m-d', 'now'),
                'salary' => $faker->numberBetween(1000000, 10000000),
                'position' => $faker->randomElement(['staff', 'manager', 'director']),
                'is_active' => $faker->boolean
            ]);
        }
    }
}
