<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('identity_number', 16);
            $table->enum('gender', ['male', 'female']);
            $table->string('email', 100);
            $table->string('phone', 13);
            $table->string('address')->nullable();
            $table->date('hire_date')->format('Y-m-d');
            $table->string('position', 100);
            $table->decimal('salary', 15, 0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
