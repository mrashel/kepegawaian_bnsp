@extends('layouts.app')

@section('chart')
    <script src="{{ $chart->cdn() }}"></script>
    {{ $chart->script() }}
@endsection

@section('content')
    <div class="container-wrapper">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card flex-fill border-0">
                    <div class="card-header">{{ __('Yearly Employees Chart') }}</div>

                    <div class="card-body">
                        {!! $chart->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
