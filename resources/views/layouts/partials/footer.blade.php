<footer class="footer container-fluid">
  <div class="row text-muted">
    <div class="col-6 text-start">
      <p class="mb-0">
        <a href="#" class="text-muted">
          <strong>Admin's Side</strong>
        </a>
      </p>
    </div>
    <div class="col-6 text-end">
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="https://youtu.be/lnWmD1cpn9M?si=A7hKBh40HMfc9gsa" target="_blank" class="text-muted">Contact</a>
        </li>
        <li class="list-inline-item">
          <a href="https://youtu.be/_ZRCXK5cbxU?si=1WX6rBn5tqlVceut" target="_blank" class="text-muted">About Us</a>
        </li>
      </ul>
    </div>
  </div>
</footer>
