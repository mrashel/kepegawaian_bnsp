<aside id="sidebar">
  <!-- Content Sidebar -->
  <div class="h-100">
      <div class="sidebar-logo">
          <a href="#">Admin's Side</a>
      </div>
      <ul class="sidebar-nav">
          <li class="sidebar-header">
              Admin Panel
          </li>
          <li class="sidebar-item">
              <a href="{{ route('home') }}" class="sidebar-link">
                  <i class="fa-solid fa-list"></i>
                  Dashboard
              </a>
          </li>
          <li class="sidebar-item">
              <a href="#" class="sidebar-link collapsed" data-bs-target="#pages"
                  data-bs-toggle="collapse" aria-expanded="false">
                  <i class="fa-solid fa-file-lines pe-2"></i>
                  Pages
              </a>
              <ul id="pages" class="sidebar-dropdown list-unstyled collapse"
                  data-bs-parent="#sidebar">
                  <li class="sidebar-item">
                      <a href="{{ route('employees.index') }}" class="sidebar-link">Employees</a>
                  </li>
                  <li class="sidebar-item">
                      <a href="#" class="sidebar-link">Next Development 🛌💤</a>
                  </li>
              </ul>
          </li>
      </ul>
  </div>
</aside>