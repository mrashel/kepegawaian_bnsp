<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-bs-theme="light">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.8/css/dataTables.bootstrap5.css">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js', 'resources/css/app.css'])

    <!-- Scripts FontAwesome -->
    <script src="https://kit.fontawesome.com/32e118131c.js" crossorigin="anonymous"></script>

    <!-- Scripts JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <!-- Scripts DataTables & Moment -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>
    <script defer src="https://cdn.datatables.net/2.0.8/js/dataTables.bootstrap5.js"></script>

</head>

<body>
    <div id="app" class="d-flex">

        @auth
            <div class="wrapper">

                <!-- Sidebar -->
                @include('layouts.partials.sidebar')

                <!-- Main Content -->
                <div class="main">
                    <nav class="navbar navbar-expand px-3 border-bottom" aria-labelledby="main-navbar">
                        <button class="btn" id="sidebar-toggle" type="button">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="navbar-collapse navbar">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="#" data-bs-toggle="dropdown" class="nav-icon pe-md-0">
                                        <img src="{{ asset('images/skylarwhite.jpg') }}" class="avatar img-fluid rounded"
                                            alt="">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end">
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                            class="dropdown-item">Log Out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <main class="content px-3 py-2">
                        <div class="container-fluid">
                            <div class="mb-3">
                                <h4>Admin Dashboard</h4>
                            </div>

                            @if (Route::currentRouteName() != 'employees.index')
                                <div class="row">
                                    <div class="col-12 d-flex">
                                        <div class="card flex-fill border-0 illustration">
                                            <div class="card-body p-0 d-flex flex-fill">
                                                <div class="row g-0 w-100">
                                                    <div class="col-6">
                                                        <div class="p-3 m-1">
                                                            <h4>Welcome Back, Admin {{ Auth::user()->name }}!</h4>
                                                            <p class="mb-0 mt-3">You are currently logged in to the admin
                                                                dashboard. This dashboard provides you with access to manage
                                                                various aspects of the system, including employee
                                                                information, stats, and more! (nah i'm lying, there's no
                                                                "more" 💀). Feel free to navigate
                                                                through the menu on the left to explore the features
                                                                available to you.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 align-self-end text-end">
                                                        <img src="{{ asset('images/bing.jpg') }}"
                                                            class="img-fluid illustration-img" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex">

                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-12 d-flex">
                                    <div class="card flex-fill border-0">
                                        <div class="card-body p-0 d-flex flex-fill">
                                            <div class="row g-0 w-100">
                                                <div class="col-12">
                                                    @yield('content')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </main>

                    <!-- Theme Toggle for next development -->
                    {{-- <a href="#" class="theme-toggle">
                        <i class="fa-regular fa-moon"></i>
                        <i class="fa-regular fa-sun"></i>
                    </a> --}}
                    <!-- Theme Toggle -->

                    <!-- Footer -->
                    @include('layouts.partials.footer')

                </div>
            </div>
        @endauth

        @guest
            <div class="w-100">
                <nav class="navbar navbar-expand-sm navbar-light bg-dark shadow-sm border-top border-bottom"
                    aria-labelledby="main-navbar">
                    <div class="container-fluid">

                        <button class="navbar-toggler border border-white" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                            <span class="navbar-toggler-icon bg-white"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <ul class="navbar-nav me-auto">

                            </ul>

                            <!-- Right Side Of Navbar -->
                            <ul class="navbar-nav ms-auto">
                                <!-- Authentication Links -->
                                @if (Route::has('login'))
                                    <li class="nav-item">
                                        <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                @endif

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link text-white"
                                            href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>

                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        @endguest

    </div>

    <!-- Chart -->
    @yield('chart')

    <!-- Scripts from Employees -->
    @yield('scripts')

    <!-- Scripts SweetAlert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>

</html>
