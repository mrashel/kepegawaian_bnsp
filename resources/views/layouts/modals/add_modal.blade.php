<!-- Modal Create -->
<div class="modal fade" id="addEmployeeModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="staticBackdropLabel">Add New Employee</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="addEmployeeForm" name="addEmployeeForm" action="{{ route('employees.store') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="employeeName" class="form-label">Name</label>
                        <input type="text" class="form-control" id="employeeName" name="name"
                            placeholder="Employee Name" autocomplete="name" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeeIdentityNumber" class="form-label">Identity Number</label>
                        <input type="text" class="form-control" id="employeeIdentityNumber" name="identity_number"
                            placeholder="Employee Identity Number" autocomplete="off" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeeGender" class="form-label">Gender</label>
                        <select class="form-control" id="employeeGender" name="gender" required>
                            <option selected value="">-- Select --</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="employeeEmail" class="form-label">Email</label>
                        <input type="email" class="form-control" id="employeeEmail" name="email"
                            placeholder="example@example.com" autocomplete="email" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeePhoneNumber" class="form-label">Phone Number</label>
                        <input type="text" class="form-control" id="employeePhoneNumber" name="phone"
                            placeholder="Employee Phone Number" autocomplete="off" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeeAddress" class="form-label">Address <span
                                class="text-muted">(*Optional)</span></label>
                        <textarea class="form-control" id="employeeAddress" name="address" placeholder="Employee Address" autocomplete="off"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="employeePosition" class="form-label">Position</label>
                        <input type="text" class="form-control" id="employeePosition" name="position"
                            placeholder="Employee Position" autocomplete="organization-title" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeeHireDate" class="form-label">Hire Date</label>
                        <input type="date" class="form-control" id="employeeHireDate" name="hire_date"
                            autocomplete="off" required>
                    </div>
                    <div class="mb-3">
                        <label for="employeeSalary" class="form-label">Salary</label>
                        <input type="number" class="form-control" id="employeeSalary" name="salary"
                            placeholder="Employee Salary" autocomplete="off" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="addEmployeeBtn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/addModal.js') }}"></script>
