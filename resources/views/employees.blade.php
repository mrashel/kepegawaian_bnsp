@extends('layouts.app')

@section('title', 'Employees')

@section('content')
    <div class="content-wrapper p-2">
        <h1>Employees</h1>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addEmployeeModal">Add
            Employee</button>
        <hr>
        <div class="table-responsive">
            <table id="employeesTable" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Hire Date</th>
                        <th>Salary</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Data from AJAX -->
                </tbody>
                <tfoot>
                    <t>
                        <th>Employee ID</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Hire Date</th>
                        <th>Salary</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- Modal for Create -->
    @include('layouts.modals.add_modal')

    <!-- Modal for Read -->
    @include('layouts.modals.read_modal')

    <!-- Modal for Update -->
    @include('layouts.modals.edit_modal')
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            // DataTable initialization with Ajax
            var table = $('#employeesTable').DataTable({
                "ajax": {
                    "url": "{{ route('employees.index') }}",
                    "type": "GET",
                    "dataSrc": "data"
                },
                "columns": [{
                        "data": "id",
                        orderable: true
                    },
                    {
                        "data": "name",
                        orderable: true
                    },
                    {
                        "data": "position",
                        orderable: true
                    },
                    {
                        "data": "email",
                        orderable: true
                    },
                    {
                        "data": "hire_date",
                        orderable: true,
                        render: function(data) {
                            return moment(data).format('YYYY-MM-DD');
                        }
                    },
                    {
                        "data": "salary",
                        orderable: true
                    },
                    {
                        "data": "is_active",
                        orderable: true,
                        "render": function(data, type, row) {
                            return data ? 'Active' : 'Inactive';
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "render": function(data, type, row) {
                            return `
                            <a href="#" class="btn btn-info show-details" data-bs-modal="modal" data-id="${row.id}">Details</a>
                            <a href="#" class="btn btn-warning" data-bs-modal="modal" data-id="${row.id}" data-bs-target="#editEmployeeModal">Edit</a>
                            <a href="#" class="btn btn-danger delete-employee" data-id="${row.id}">Delete</a>
                        `;
                        }
                    }
                ],
                "lengthMenu": [
                    [5, 15, 30, -1],
                    [5, 15, 30, "All"]
                ],
                "pageLength": 5,
                "responsive": true,
            });

            // Handle delete button
            $('#employeesTable').on('click', '.delete-employee', function(e) {
                e.preventDefault();
                var employeeId = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Yes, delete it !',
                    cancelButtonText: 'Nuh uh, cancel !'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: `/panel/employee/${employeeId}/destroy`,
                            type: 'GET',
                            success: function(response) {
                                Swal.fire(
                                    'Deleted!',
                                    'Employee has been deleted.',
                                    'success'
                                );
                                $('#employeesTable').DataTable().ajax.reload();
                            },
                            error: function(response) {
                                console.error(response);
                                Swal.fire(
                                    'Error!',
                                    'Failed to delete employee. Please try again.',
                                    'error'
                                );
                            }
                        });
                    }
                });
            });

            // Refresh table when modal closed
            $('#addEmployeeModal, #editEmployeeModal').on('hidden.bs.modal', function() {
                table.ajax.reload();
            });
        });
    </script>
@endsection
